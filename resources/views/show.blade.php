@extends('layouts.app')

@section('content')

<router-view
        @if(Auth::check())
        :user=true
        @else :user=false
        @endif>
</router-view>

@endsection
