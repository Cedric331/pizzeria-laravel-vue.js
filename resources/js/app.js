/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router'


import Home from './components/HomeComponent.vue';
import Show from './components/ShowComponent.vue';
import Panier from './components/PanierComponent.vue';

Vue.use(VueRouter)
const routes = [
    {
        path: '/',
        component: Home,
    },
    {
        path: '/show/:name',
        component: Show,
        props: true
    },
    {
        path: '/compte/panier',
        component: Show,
        props: true
    },
  ]

const routeur = new VueRouter({routes});


const app = new Vue({
    el: '#app',
    router: routeur
});
