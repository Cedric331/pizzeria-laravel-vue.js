<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pizza_Ingredient extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pizza_id', 'ingredient_id',
    ];

        /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'pizza_id', 'ingredient_id', 'created_at', 'updated_at', 'id'
    ];
}
