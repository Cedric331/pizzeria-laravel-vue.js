<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pizza extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'picture', 'amount',
    ];

    public function ingredient(){
        return $this->hasMany(Pizza_Ingredient::class)
                    ->join('ingredients', 'ingredients.id', 'ingredient_id');
    }
}
